import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dishes';
  public selectedMenuItem = 'Recipe';
  selectedMenu(menuItem: string) {
    this.selectedMenuItem = menuItem;
  }
}
