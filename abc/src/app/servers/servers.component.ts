import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.scss']
})
export class ServersComponent implements OnInit {
welcomeText: string = "-No Server Details-";
btnStatus: boolean = false;
serverName: string = '';
serverList = ['Server #1', 'Server #2'];
loading: boolean = false;
serverStatus: any ='';

  constructor() {
    setTimeout(() => {
      this.btnStatus = true;
    }, 2000);
    
  }

  ngOnInit(): void {}

  addStatus(){
    this.loading= true;
    this.serverList.push(this.serverName);
    this.serverStatus = Math.random()> .5 ? 'online' : 'offline';
  }
  // getBgColor(){
  //   return this.serverStatus === 'online'? 'green': 'red';
  // }


}
