import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.scss']
})
export class AssignmentComponent implements OnInit {
  userName: string = '';
  constructor() { }

  ngOnInit(): void {
  }
  resetName(){
    this.userName = "";
  }
}
