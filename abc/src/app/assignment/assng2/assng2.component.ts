import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assng2',
  templateUrl: './assng2.component.html',
  styleUrls: ['./assng2.component.scss']
})
export class Assng2Component implements OnInit {
flag:boolean = false;
btnClick= [];
i: number =0;

  constructor() { }

  ngOnInit(): void {
  }
  showDetails(){
    this.flag = !this.flag;
    this.btnClick.push('count number:' + this.i++ +", Date:" + new Date());
  }
  checkLog(i){
    return i>4 ? 'blue': 'transparent';
  }
}
